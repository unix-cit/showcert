# showcert

Tool to display information about certificates in various situations.
```
Usage:
showcert [ host:port[:starttlsprotocol] | file | https://sphost/Shibboleth.sso/Metadata ] [-cert] [ x509_arg ... ] 
#
-cert indicates to output certificates (in PEM format)
Possible openssl x509 arguments are 
 -issuer -subject -dates -fingerprint -serial -text
If a starttlsprotocol is specified the connection will initiate STARTTLS for that protocol
 supported values are "smtp", "pop3", "imap", "ftp" and "xmpp" (cf. option -starttls in "man s_client")

Examples:
  showcert www.unige.ch:443 -cert
  showcert ca-bundle.pem -subject
  showcert https://elearn-labs.unige.ch/Shibboleth.sso/Metadata
  showcert metadata.switchaai.xml
  showcert smtp.unige.ch:25:smtp
```

